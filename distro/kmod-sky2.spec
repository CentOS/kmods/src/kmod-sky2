%global pkg sky2

%global kernel 4.18.0-553.el8_10
%global baserelease 1

%global _use_internal_dependency_generator 0
%global __find_requires /usr/lib/rpm/redhat/find-requires
%global __find_provides /usr/lib/rpm/redhat/find-provides

%global debug_package %{nil}

%global __spec_install_post \
  %{?__debug_package:%{__debug_install_post}} \
  %{__arch_install_post} \
  %{__os_install_post} \
  %{__mod_compress_install_post}

%global __mod_compress_install_post %{nil}


Name:             kmod-%{pkg}
Epoch:            1
Version:          %(echo %{kernel} | sed 's/-/~/g; s/\.el.*$//g')
Release:          %{baserelease}%{?dist}
Summary:          Marvell Yukon 2 Gigabit Ethernet (%{pkg}) driver

License:          GPLv2
URL:              https://www.kernel.org/

Patch0:           source-git.patch

ExclusiveArch:    x86_64 aarch64 ppc64le

BuildRequires:    elfutils-libelf-devel
BuildRequires:    gcc
BuildRequires:    kernel-rpm-macros
BuildRequires:    kmod
BuildRequires:    make
BuildRequires:    redhat-rpm-config

BuildRequires:    kernel-abi-stablelists = %{kernel}
BuildRequires:    kernel-devel = %{kernel}
BuildRequires:    kernel-devel-uname-r = %{kernel}.%{_arch}

Requires:         kernel >= %{kernel}
Requires:         kernel-uname-r >= %{kernel}.%{_arch}
Requires:         kernel-modules >= %{kernel}
Requires:         kernel-modules-uname-r >= %{kernel}.%{_arch}

Provides:         installonlypkg(kernel-module)
Provides:         kernel-modules >= %{kernel}.%{_arch}

Requires(posttrans): %{_sbindir}/depmod
Requires(postun):    %{_sbindir}/depmod

Requires(posttrans): %{_sbindir}/weak-modules
Requires(postun):    %{_sbindir}/weak-modules

Requires(posttrans): %{_bindir}/sort
Requires(postun):    %{_bindir}/sort


%if %{epoch}
Obsoletes:        kmod-%{pkg} < %{epoch}:
%endif
Obsoletes:        kmod-%{pkg} = %{?epoch:%{epoch}:}%{version}


%description
This driver supports Gigabit Ethernet adapters based on the Marvell Yukon 2
chipset:

- 0x1148:0x9000: SysKonnect SK-9Sxx
- 0x1148:0x9E00: SysKonnect SK-9Exx
- 0x1148:0x9E01: SysKonnect SK-9E21M
- 0x1186:0x4001: D-Link DGE-550SX
- 0x1186:0x4B00: D-Link DGE-560T
- 0x1186:0x4B02: D-Link DGE-560SX
- 0x1186:0x4B03: D-Link DGE-550T
- 0x11AB:0x4340: Marvell 88E8021
- 0x11AB:0x4341: Marvell 88E8022
- 0x11AB:0x4342: Marvell 88E8061
- 0x11AB:0x4343: Marvell 88E8062
- 0x11AB:0x4344: Marvell 88E8021
- 0x11AB:0x4345: Marvell 88E8022
- 0x11AB:0x4346: Marvell 88E8061
- 0x11AB:0x4347: Marvell 88E8062
- 0x11AB:0x4350: Marvell 88E8035
- 0x11AB:0x4351: Marvell 88E8036
- 0x11AB:0x4352: Marvell 88E8038
- 0x11AB:0x4353: Marvell 88E8039
- 0x11AB:0x4354: Marvell 88E8040
- 0x11AB:0x4355: Marvell 88E8040T
- 0x11AB:0x4356: Marvell 88EC033
- 0x11AB:0x4357: Marvell 88E8042
- 0x11AB:0x435A: Marvell 88E8048
- 0x11AB:0x4360: Marvell 88E8052
- 0x11AB:0x4361: Marvell 88E8050
- 0x11AB:0x4362: Marvell 88E8053
- 0x11AB:0x4363: Marvell 88E8055
- 0x11AB:0x4364: Marvell 88E8056
- 0x11AB:0x4365: Marvell 88E8070
- 0x11AB:0x4366: Marvell 88EC036
- 0x11AB:0x4367: Marvell 88EC032
- 0x11AB:0x4368: Marvell 88EC034
- 0x11AB:0x4369: Marvell 88EC042
- 0x11AB:0x436A: Marvell 88E8058
- 0x11AB:0x436B: Marvell 88E8071
- 0x11AB:0x436C: Marvell 88E8072
- 0x11AB:0x436D: Marvell 88E8055
- 0x11AB:0x4370: Marvell 88E8075
- 0x11AB:0x4380: Marvell 88E8057
- 0x11AB:0x4381: Marvell 88E8059
- 0x11AB:0x4382: Marvell 88E8079


%prep
%autosetup -p1 -c -T


%build
pushd src
%{__make} -C /usr/src/kernels/%{kernel}.%{_arch} %{?_smp_mflags} M=$PWD modules
popd


%install
%{__install} -D -t %{buildroot}/lib/modules/%{kernel}.%{_arch}/extra/drivers/net/ethernet/marvell src/%{pkg}.ko

# Make .ko objects temporarily executable for automatic stripping
find %{buildroot}/lib/modules -type f -name \*.ko -exec chmod u+x \{\} \+


%clean
%{__rm} -rf %{buildroot}


%triggerin -- %{name}
mkdir -p %{_localstatedir}/lib/rpm-state/sig-kmods
printf '%s\n' "/lib/modules/%{kernel}.%{_arch}/extra/drivers/net/ethernet/marvell/%{pkg}.ko" >> %{_localstatedir}/lib/rpm-state/sig-kmods/weak-modules


%triggerun -- %{name}
mkdir -p %{_localstatedir}/lib/rpm-state/sig-kmods
printf '%s\n' "/lib/modules/%{kernel}.%{_arch}/extra/drivers/net/ethernet/marvell/%{pkg}.ko" >> %{_localstatedir}/lib/rpm-state/sig-kmods/weak-modules


%preun
mkdir -p %{_localstatedir}/lib/rpm-state/sig-kmods
rpm -ql kmod-%{pkg}-%{?epoch:%{epoch}:}%{version}-%{release}.%{_arch} | grep '/lib/modules/%{kernel}.%{_arch}/.*\.ko$' >> %{_localstatedir}/lib/rpm-state/sig-kmods/weak-modules-remove


%postun
if [ -f %{_localstatedir}/lib/rpm-state/sig-kmods/weak-modules ]
then
    while read -r MODULE
    do
        if [ -f "$MODULE" ]
        then
            printf '%s\n' "$MODULE" >> %{_localstatedir}/lib/rpm-state/sig-kmods/weak-modules-add
        fi
    done < %{_localstatedir}/lib/rpm-state/sig-kmods/weak-modules
    rm -f %{_localstatedir}/lib/rpm-state/sig-kmods/weak-modules
fi
if [ -f %{_localstatedir}/lib/rpm-state/sig-kmods/weak-modules-remove ]
then
    modules=( $(cat %{_localstatedir}/lib/rpm-state/sig-kmods/weak-modules-remove | sort -u -V -t '/' -k 5 -k 4,4) )
    rm -f %{_localstatedir}/lib/rpm-state/sig-kmods/weak-modules-remove
    if [ -f %{_localstatedir}/lib/rpm-state/sig-kmods/weak-modules-add ]
    then
        printf '%s\n' "${modules[@]}" | %{_sbindir}/weak-modules --remove-modules --no-initramfs
    else
        printf '%s\n' "${modules[@]}" | %{_sbindir}/weak-modules --remove-modules
    fi
fi
if [ -f %{_localstatedir}/lib/rpm-state/sig-kmods/weak-modules-add ]
then
    modules=( $(cat %{_localstatedir}/lib/rpm-state/sig-kmods/weak-modules-add | sort -u -V -t '/' -k 5 -k 4,4) )
    rm -f %{_localstatedir}/lib/rpm-state/sig-kmods/weak-modules-add
    printf '%s\n' "${modules[@]}" | %{_sbindir}/weak-modules --add-modules
fi
if [ -d %{_localstatedir}/lib/rpm-state/sig-kmods ]
then
    rmdir --ignore-fail-on-non-empty %{_localstatedir}/lib/rpm-state/sig-kmods
fi


%posttrans
if [ -f %{_localstatedir}/lib/rpm-state/sig-kmods/weak-modules ]
then
    while read -r MODULE
    do
        if [ -f "$MODULE" ]
        then
            printf '%s\n' "$MODULE" >> %{_localstatedir}/lib/rpm-state/sig-kmods/weak-modules-add
        fi
    done < %{_localstatedir}/lib/rpm-state/sig-kmods/weak-modules
    rm -f %{_localstatedir}/lib/rpm-state/sig-kmods/weak-modules
fi
if [ -f %{_localstatedir}/lib/rpm-state/sig-kmods/weak-modules-add ]
then
    modules=( $(cat %{_localstatedir}/lib/rpm-state/sig-kmods/weak-modules-add | sort -u -V -t '/' -k 5 -k 4,4) )
    rm -f %{_localstatedir}/lib/rpm-state/sig-kmods/weak-modules-add
    printf '%s\n' "${modules[@]}" | %{_sbindir}/weak-modules --add-modules
fi
if [ -d %{_localstatedir}/lib/rpm-state/sig-kmods ]
then
    rmdir --ignore-fail-on-non-empty %{_localstatedir}/lib/rpm-state/sig-kmods
fi


%files
%defattr(644,root,root,755)
/lib/modules/%{kernel}.%{_arch}
%license licenses


%changelog
* Thu May 23 2024 Kmods SIG <sig-kmods@centosproject.org> - 1:4.18.0~553-1
- kABI tracking kmod package (kernel >= 4.18.0-553.el8_10)
